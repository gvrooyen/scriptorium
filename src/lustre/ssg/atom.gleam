//// Bindings for Atom feed elements.

import lustre/attribute.{attribute}
import lustre/element.{type Element, advanced, element, namespaced, text}

pub fn feed(children: List(Element(a))) {
  element("feed", [attribute("xmlns", "http://www.w3.org/2005/Atom")], children)
}

pub fn entry(children: List(Element(a))) {
  element("entry", [], children)
}

pub fn id(uri: String) {
  element("id", [], [text(uri)])
}

pub fn title(title: String) {
  element("title", [attribute("type", "html")], [text(title)])
}

pub fn updated(iso_timestamp: String) {
  element("updated", [], [text(iso_timestamp)])
}

pub fn published(iso_timestamp: String) {
  element("published", [], [text(iso_timestamp)])
}

pub fn author(children: List(Element(a))) {
  element("author", [], children)
}

pub fn contributor(children: List(Element(a))) {
  element("contributor", [], children)
}

pub fn source(children: List(Element(a))) {
  namespaced("", "source", [], children)
}

pub fn link(attributes: List(attribute.Attribute(a))) {
  advanced("", "link", attributes, [], True, False)
}

pub fn name(name: String) {
  element("name", [], [text(name)])
}

pub fn email(email: String) {
  element("email", [], [text(email)])
}

pub fn uri(uri: String) {
  element("uri", [], [text(uri)])
}

pub fn category(attributes: List(attribute.Attribute(a))) {
  advanced("", "category", attributes, [], True, False)
}

pub fn generator(attributes: List(attribute.Attribute(a)), name: String) {
  element("generator", attributes, [text(name)])
}

pub fn icon(path: String) {
  element("icon", [], [text(path)])
}

pub fn logo(path: String) {
  element("logo", [], [text(path)])
}

pub fn rights(rights: String) {
  element("rights", [], [text(rights)])
}

pub fn subtitle(subtitle: String) {
  element("subtitle", [attribute("type", "html")], [text(subtitle)])
}

pub fn summary(summary: String) {
  element("summary", [attribute("type", "html")], [text(summary)])
}

pub fn content(content: String) {
  element("content", [attribute("type", "html")], [text(content)])
}
