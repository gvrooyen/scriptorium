# Changelog

## 1.0.2

Adjusted the title sizing.

## 1.0.1

Fix dependency to `lustre_ssg`.

## 1.0.0

Initial release.
