import scriptorium/compiler.{type CompiledPage}
import scriptorium/config.{type Configuration}
import scriptorium/models/database.{type Database}
import lustre/attribute.{attribute, class}
import lustre/element.{text}
import lustre/element/html.{article, div, h2, header}

pub fn generate(db: Database, config: Configuration) {
  view(_, db, config)
}

fn view(page: CompiledPage, _db: Database, _config: Configuration) {
  article([class("page")], [
    header([], [h2([], [text(page.orig.title)])]),
    div([attribute("dangerous-unescaped-html", page.content)], []),
  ])
}
