/// A post or page header: a string key and a string value.
pub type Header =
  #(String, String)
