# scriptorium

Scriptorium is a simple blog generator for Gleam, using [Lustre](https://hexdocs.pm/lustre) and
[lustre_ssg](https://hexdocs.pm/lustre_ssg).

Scriptorium runs on the JavaScript target and it's tested with Node.js.

[![Package Version](https://img.shields.io/hexpm/v/scriptorium)](https://hex.pm/packages/scriptorium)
[![Hex Docs](https://img.shields.io/badge/hex-docs-ffaff3)](https://hexdocs.pm/scriptorium/)

## Quickstart

To use Scriptorium, you will need to create a new Gleam project and add Scriptorium as a
dependency.

```sh
gleam add scriptorium
```

To generate a default blog, use the following in your main file:

```gleam
import gleam/result
import gleam/option
import gleam/io
import scriptorium/builder
import scriptorium/config.{type Configuration, Configuration}
import scriptorium/defaults

pub fn main() {
  let config =
    defaults.default_config(
      "My Blog",
      "https://my.blog.example/",
      "en",
      config.Author(
        "Person McPerson",
        option.Some("person@example.com"),
        option.Some("https://fedi.instance.example/@person"),
      ),
      "© Person McPerson",
    )

  io.debug(build(config))
}

pub fn build(config: Configuration) {
  // Parse the files
  use db <- result.try(builder.parse(config))
  // Compile Markdown into HTML
  let compiled = builder.compile(db, config)
  // Render content into Lustre elements
  let rendered = builder.render(db, compiled, config)
  // Write rendered content into the filesystem
  builder.write(rendered, config)
}
```

Now you will need to add source files. Create the following directory tree in the root of the project
folder:

```
your_project
└── data
    ├── pages
    └── posts
```

In `./data/posts`, create a file `2024-04-21-hello-world.md` with the following contents:

```markdown
Hello, World!
meta

This is the first blog entry.
```

Then run `gleam run --target javascript` to build the blog and see what was generated in `./output`.

For further usage help, see the documentation as described below.

## Documentation

- [User's Guide](https://nicd.gitlab.io/scriptorium_blog/guide.html)
- [Scriptorium API reference](https://hexdocs.pm/scriptorium)
- [Scriptorium Demo Blog](https://nicd.gitlab.io/scriptorium_blog)
