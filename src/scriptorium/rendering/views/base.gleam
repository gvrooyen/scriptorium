import gleam/dict
import gleam/float
import gleam/int
import gleam/list
import gleam/option
import gleam/string
import scriptorium/config.{type Configuration}
import scriptorium/models/database.{type Database}
import scriptorium/models/menu.{type MenuItem}
import scriptorium/utils/date
import scriptorium/utils/ordered_tree
import lustre/attribute.{attribute, href, id, name, rel, role, style, type_}
import lustre/element.{type Element, text}
import lustre/element/html.{
  a, body, footer, h1, head, header, html, li, link, main, meta, nav, p, section,
  title, ul,
}

const tag_min_size = 0.5

/// The base view pre-renders some content once, so that it can be reused for every render.
pub type PreRendered {
  PreRendered(pages: Element(Nil), tags: Element(Nil), archives: Element(Nil))
}

pub fn generate(db: Database, config: Configuration) {
  let pre_rendered = pre_render(db, config)
  fn(inner: Element(Nil), extra_meta: List(Element(Nil)), title_prefix: String) {
    view(db, config, pre_rendered, inner, extra_meta, title_prefix)
  }
}

fn pre_render(db, config) -> PreRendered {
  PreRendered(
    pages: element.none(),
    tags: tags(db, config),
    archives: archives(db, config),
  )
}

fn view(
  db: Database,
  config: Configuration,
  pre_rendered: PreRendered,
  inner: Element(Nil),
  extra_meta: List(Element(Nil)),
  title_prefix: String,
) {
  let title_text = case title_prefix {
    "" -> config.blog_name
    prefix -> prefix <> " · " <> config.blog_name
  }

  html([attribute("lang", config.language)], [
    head([], [
      meta([attribute("charset", "utf-8")]),
      meta([
        name("viewport"),
        attribute("content", "width=device-width, initial-scale=1"),
      ]),
      title([], title_text),
      link([href(config.paths.root <> "/css/normalize.css"), rel("stylesheet")]),
      link([href(config.paths.root <> "/css/magick.css"), rel("stylesheet")]),
      link([href(config.paths.root <> "/css/custom.css"), rel("stylesheet")]),
      link([
        href(config.paths.root <> config.paths.feed),
        rel("alternate"),
        type_("application/atom+xml"),
      ]),
      case config.author.url {
        option.Some(url) -> link([rel("me"), href(url)])
        _ -> element.none()
      },
      ..extra_meta
    ]),
    body([], [
      header([id("title"), role("banner")], [
        h1([], [
          a([href(config.paths.html(config.paths.root <> config.paths.index))], [
            text(config.blog_name),
          ]),
        ]),
        case database.menu(db) {
          [] -> element.none()
          m -> menu(m)
        },
      ]),
      main([], [inner]),
      section([id("sidebar")], [
        case dict.size(database.tags(db)) {
          0 -> element.none()
          _ ->
            nav([id("tags"), attribute("aria-label", "Tags")], [
              pre_rendered.tags,
            ])
        },
        nav([id("archives"), attribute("aria-label", "Archives")], [
          pre_rendered.archives,
        ]),
      ]),
      footer([], [
        p([], [
          text(config.rendering.copyright),
          text(" · "),
          text("Powered by: "),
          a([href("https://gleam.run/")], [text("Gleam")]),
          text(", "),
          a([href("https://hexdocs.pm/lustre")], [text("Lustre")]),
          text(", "),
          a([href("https://gitlab.com/Nicd/scriptorium")], [text("Scriptorium")]),
        ]),
      ]),
    ]),
  ])
}

fn tags(db: Database, config: Configuration) {
  let tags =
    db
    |> database.tags()
    |> dict.map_values(fn(_key, posts) {
      int.to_float(ordered_tree.length(posts))
    })

  let most_posts =
    tags
    |> dict.values()
    |> list.fold(0.0, float.max)

  tags
  |> dict.to_list()
  |> list.sort(fn(a, b) {
    string.compare(string.lowercase(a.0), string.lowercase(b.0))
  })
  |> list.map(fn(item) {
    let #(tag, post_count) = item
    let percentage =
      float.round(
        {
          { { post_count /. most_posts } *. { 1.0 -. tag_min_size } }
          +. tag_min_size
        }
        *. 100.0,
      )

    li([], [
      a(
        [
          href(config.paths.html(config.paths.root <> config.paths.tag(tag))),
          style([#("font-size", int.to_string(percentage) <> "%")]),
        ],
        [text(tag)],
      ),
    ])
  })
  |> ul([], _)
}

fn archives(db: Database, config: Configuration) {
  db
  |> database.years()
  |> dict.to_list()
  |> list.sort(fn(a, b) { int.compare(a.0, b.0) })
  |> list.fold([], fn(year_archive, year) {
    let #(year, months) = year
    [
      li([], [
        a(
          [
            href(config.paths.html(
              config.paths.root
              <> config.paths.list_page(config.paths.year(year), 1),
            )),
          ],
          [text(int.to_string(year))],
        ),
        ul(
          [],
          list.fold(date.months, [], fn(month_archive, month) {
            case dict.get(months, month) {
              Ok(posts) -> [
                li([], [
                  a(
                    [
                      href(config.paths.html(
                        config.paths.root
                        <> config.paths.list_page(
                          config.paths.month(year, month),
                          1,
                        ),
                      )),
                    ],
                    [
                      text(
                        date.month_to_string(month)
                        <> " ("
                        <> int.to_string(ordered_tree.length(posts))
                        <> ")",
                      ),
                    ],
                  ),
                ]),
                ..month_archive
              ]
              Error(_) -> month_archive
            }
          }),
        ),
      ]),
      ..year_archive
    ]
  })
  |> ul([], _)
}

fn menu(menu: List(MenuItem)) {
  nav([], [
    ul(
      [],
      list.map(menu, fn(item) {
        li([], [a([href(item.url)], [text(item.name)])])
      }),
    ),
  ])
}
