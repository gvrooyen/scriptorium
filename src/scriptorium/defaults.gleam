//// This module contains default configurations that can be used to create a
//// blog quickly.

import gleam/uri
import gleam/string
import scriptorium/compiler
import scriptorium/config.{type Configuration, Compiling, Configuration, Rendering}
import scriptorium/parser
import scriptorium/paths
import scriptorium/renderer
import scriptorium/rendering/views/base
import scriptorium/rendering/views/feed
import scriptorium/rendering/views/list_page
import scriptorium/rendering/views/meta
import scriptorium/rendering/views/page
import scriptorium/rendering/views/single_post
import scriptorium/writer

/// View generators that use the Scriptorium builtin views.
const default_views = config.Views(
  base: base.generate,
  meta: meta.generate,
  single_post_full: single_post.full_view,
  single_post_list: single_post.list_view,
  page: page.generate,
  list_page: list_page.generate,
  feed: feed.generate,
)

/// Get a sensible default configuration based on the given arguments.
pub fn default_config(
  blog_name: String,
  blog_url: String,
  language: String,
  author: config.Author,
  copyright: String,
) -> Configuration {

  // Drop any trailing slash from the blog URL, otherwise a `blog_url` like
  // "https://my.blog.example/" will end up having a root path of "/" instead
  // of "".
  let blog_url = case string.ends_with(blog_url, "/") {
    True -> string.drop_right(blog_url, 1)
    False -> blog_url
  }
  let assert Ok(parsed_url) = uri.parse(blog_url)

  Configuration(
    blog_name: blog_name,
    blog_url: blog_url,
    language: language,
    author: author,
    compiling: Compiling(
      database_compiler: compiler.compile,
      item_compiler: compiler.default_compiler,
    ),
    rendering: Rendering(
      renderer: renderer.render,
      views: default_views,
      copyright: copyright,
      posts_per_page: 10,
      posts_in_feed: 20,
    ),
    paths: paths.PathConfiguration(..paths.defaults, root: parsed_url.path),
    parser: parser.default_parse,
    writer: writer.write,
    output_path: "./output",
  )
}
