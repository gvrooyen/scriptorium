//// A static page in the blog that is not part of any post lists or archives.

import scriptorium/models/header.{type Header}

pub type Page {
  Page(title: String, slug: String, headers: List(Header), content: String)
}
