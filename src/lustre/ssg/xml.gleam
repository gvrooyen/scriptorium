import lustre/element.{advanced}

/// Generate an XML declaration.
pub fn declaration() {
  advanced("", "?xml version=\"1.0\" encoding=\"utf-8\"?", [], [], False, True)
}
