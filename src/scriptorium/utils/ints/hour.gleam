//// An hour is a ranged integer from 0 to 23.

import bigi.{type BigInt}
import ranged_int/interface.{type Interface, Interface}

const max_limit = 23

const min_limit = 0

const iface: Interface(Hour, interface.Overflowable) = Interface(
  from_bigint_unsafe: from_bigint_unsafe,
  to_bigint: to_bigint,
  limits: limits,
)

pub opaque type Hour {
  Hour(data: BigInt)
}

fn to_bigint(value: Hour) {
  value.data
}

pub fn to_int(value: Hour) {
  let assert Ok(int) = bigi.to_int(to_bigint(value))
  int
}

pub fn from_int(value: Int) {
  interface.from_bigint(bigi.from_int(value), iface)
}

pub fn compare(a: Hour, b: Hour) {
  interface.compare(a, b, iface)
}

fn limits() {
  interface.overflowable_limits(
    bigi.from_int(min_limit),
    bigi.from_int(max_limit),
  )
}

fn from_bigint_unsafe(value: BigInt) {
  Hour(data: value)
}
