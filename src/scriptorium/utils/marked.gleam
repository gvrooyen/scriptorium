//// Bindings to the marked.js library.

import scriptorium/internal/utils/object.{type Object}

/// Marked.js options object.
pub type Options =
  Object

/// Create a new options object.
pub fn new_options() -> Options {
  object.new()
}

/// Set the `mangle` option on or off.
pub fn set_mangle(options: Options, do_mangle: Bool) -> Options {
  object.set(options, "mangle", do_mangle)
}

/// Set the `headerIds` option on or off.
pub fn set_header_ids(options: Options, ids: Bool) -> Options {
  object.set(options, "headerIds", ids)
}

/// Set the `headerPrefix` option.
pub fn set_header_prefix(options: Options, prefix: String) -> Options {
  object.set(options, "headerPrefix", prefix)
}

/// Parse a string containing Markdown using the default options.
pub fn default_parse(content: String) -> String {
  let options =
    new_options()
    |> set_mangle(False)
    |> set_header_ids(False)
    |> set_header_prefix("")

  parse(content, options)
}

/// Parse a string containing Markdown using Marked.js.
@external(javascript, "../../priv/vendor/marked.esm.mjs", "parse")
pub fn parse(content content: String, options options: Options) -> String
