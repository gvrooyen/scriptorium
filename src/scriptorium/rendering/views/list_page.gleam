import gleam/list
import scriptorium/config.{type Configuration}
import scriptorium/models/database.{type Database}
import scriptorium/rendering/views.{type ListInfo}
import scriptorium/rendering/views/nav
import lustre/attribute.{attribute, class}
import lustre/element
import lustre/element/html.{footer, nav}

pub fn generate(db: Database, config: Configuration) {
  let single_post_renderer = config.rendering.views.single_post_list(db, config)

  fn(info: ListInfo) {
    let none = element.none()

    let footer = case info.total_pages > 1 {
      True ->
        nav([class("page-nav"), attribute("aria-label", "Pages")], [
          footer([], [nav.view(info, info.root_path, config)]),
        ])
      False -> none
    }

    html.section(
      [class("post-list")],
      list.flatten([
        case info.extra_header {
          el if el == none -> []
          el -> [el]
        },
        list.map(info.posts, single_post_renderer),
        [footer],
      ]),
    )
  }
}
