pub type Object

@external(javascript, "../../../ffi_object.mjs", "create")
pub fn new() -> Object

@external(javascript, "../../../ffi_object.mjs", "set")
pub fn set(object object: Object, prop prop: String, value value: a) -> Object

@external(javascript, "../../../ffi_object.mjs", "get")
pub fn get(object object: Object, prop prop: String) -> b
