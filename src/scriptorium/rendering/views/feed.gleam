import gleam/list
import gleam/option
import scriptorium/compiler.{type CompiledPost}
import scriptorium/config.{type Configuration}
import scriptorium/models/database.{type Database}
import scriptorium/models/post
import scriptorium/utils/luxon
import lustre/attribute.{attribute}
import lustre/element
import lustre/ssg/atom.{
  author, content, email, entry, feed, generator, id, link, name, rights,
  summary, title, updated, uri,
}

pub fn generate(_db: Database, config: Configuration) {
  fn(posts: List(CompiledPost)) {
    let now = luxon.utc_now()
    let #(posts, _) = list.split(posts, config.rendering.posts_in_feed)

    feed([
      title(config.blog_name),
      updated(luxon.to_iso(now)),
      link([attribute("href", config.blog_url)]),
      id(config.blog_url),
      author([
        name(config.author.name),
        case config.author.email {
          option.Some(e) -> email(e)
          option.None -> element.none()
        },
        case config.author.url {
          option.Some(u) -> uri(u)
          option.None -> element.none()
        },
      ]),
      generator(
        [
          attribute("uri", "https://gitlab.com/Nicd/scriptorium"),
          attribute("version", "1.0.0"),
        ],
        "Scriptorium",
      ),
      rights(config.rendering.copyright),
      ..list.map(posts, fn(post) {
        let date_str = post.to_iso8601(post.orig)

        entry([
          title(post.orig.title),
          id(
            config.blog_url
            <> config.paths.html(config.paths.single_post(post.orig)),
          ),
          updated(date_str),
          content(post.content.full),
          case post.content.short {
            option.Some(s) -> summary(s)
            option.None -> element.none()
          },
        ])
      })
    ])
  }
}
