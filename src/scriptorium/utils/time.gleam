import gleam/int
import gleam/order.{type Order, Eq}
import gleam/string
import scriptorium/utils/ints/hour.{type Hour}
import scriptorium/utils/ints/minute.{type Minute}

pub type Time {
  Time(hours: Hour, minutes: Minute)
}

/// Compare if `a` is before (lower than) than `b`.
pub fn compare(a: Time, b: Time) -> Order {
  case hour.compare(a.hours, b.hours) {
    Eq -> minute.compare(a.minutes, b.minutes)
    other -> other
  }
}

/// Parse a time from an `hh:mm` format string.
pub fn parse(str: String) {
  case string.split(str, ":") {
    [hours, minutes] ->
      case int.parse(hours), int.parse(minutes) {
        Ok(h), Ok(m) ->
          case hour.from_int(h), minute.from_int(m) {
            Ok(h), Ok(m) -> Ok(Time(h, m))
            _, _ -> Error(Nil)
          }
        _, _ -> Error(Nil)
      }
    _ -> Error(Nil)
  }
}

/// Format a time to an `hh:mm` format string.
pub fn format(time: Time) -> String {
  pad(int.to_string(hour.to_int(time.hours)))
  <> ":"
  <> pad(int.to_string(minute.to_int(time.minutes)))
}

/// Get a time at hour 0 and minute 0.
pub fn nil_time() {
  let assert Ok(h) = hour.from_int(0)
  let assert Ok(m) = minute.from_int(0)
  Time(h, m)
}

fn pad(part: String) {
  string.pad_left(part, 2, "0")
}
